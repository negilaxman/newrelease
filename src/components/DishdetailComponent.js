import React from "react";
import { Menu } from "./MenuComponent";
import { debug } from "util";
import ModelPopup from "./modelpopup";
import { Link } from "react-router-dom";
import {
    Media,
    Card,
    CardImg,
    CardTitle,
    CardBody,
    CardImgOverlay,
    CardText,
    Breadcrumb,
    BreadcrumbItem,
} from "reactstrap";

function RenderDish({ dish }) {
    return (
        <div className="col-12 col-md-4 ml-5 mr-3 mb-5">
            <CardBody>
                <CardTitle>{dish.name}</CardTitle>
            </CardBody>

            <Card>
                <CardImg top src={dish.image} alt={dish.title} />
            </Card>
        </div>
    );
}

function RenderComments({ comments }) {
    if (comments != null) {
        return (
            <div className="col-12 col-md-4 m-1">
                <h4>Comments</h4>
                <ul className="list-unstyled">
                    {comments.map(comment => {
                        return (
                            <li key={comment.id}>
                                <p>{comment.comment}</p>
                                <p>{comment.author}</p>
                            </li>
                        );
                    })}
                </ul>
                <ModelPopup />
            </div>
        );
    }
}

const DishDetail = props => {
    if (props.dish != null) {
        return (
            <div classNam="container">
                <div className="row brd">
                    <Breadcrumb>
                        <BreadcrumbItem>
                            <Link to="/home">Home</Link>
                        </BreadcrumbItem>
                        <BreadcrumbItem active>Menu</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>Item Detial</h3>
                    </div>
                </div>
                <hr />
                <div className="row">
                    <RenderDish dish={props.dish} />
                    <RenderComments comments={props.comments} />
                </div>
            </div>
        );
    }
};

export default DishDetail;
