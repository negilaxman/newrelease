import React from "react";
import ModelPopup from "./modelpopup";

import {
    Media,
    Card,
    CardImg,
    CardTitle,
    CardBody,
    CardImgOverlay,
    CardText,
} from "reactstrap";
import { Link } from "react-router-dom";
import { relative } from "path";

class ListComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            item: null,
        };
    }

    render() {
        const listelement = this.props.comments.map(list => {
            return (
                <div className="col-12 col-md-3 m-1">
                    <Card>
                        <CardTitle heading>{list.comment}</CardTitle>
                    </Card>
                </div>
            );
        });

        return (
            <div>
                <div>{listelement}</div>
            </div>
        );
    }
}
export default ListComponent;
