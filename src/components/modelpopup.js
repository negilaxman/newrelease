import React from "react";
import {
    ModalHeader,
    Modal,
    ModalFooter,
    ModalBody,
    Button,
    Row,
    Col,
    Label,
} from "reactstrap";
import { Control, LocalForm, Errors } from "react-redux-form";

const required = val => val && val.length;
const maxLength = len => val => !val || val.length <= len;
const minLength = len => val => val && val.length >= len;

class ModelPopup extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            model: false,
        };
        this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState(prevState => ({
            modal: !prevState.modal,
        }));
    }
    handleSubmit(values) {
        alert("Current State is: " + JSON.stringify(values));
    }
    render() {
        return (
            <div>
                <Button color="danger" onClick={this.toggle}>
                    Make Comment
                </Button>
                <Modal
                    isOpen={this.state.modal}
                    toggle={this.toggle}
                    className={this.props.className}
                >
                    <ModalHeader toggle={this.toggle}>Make comment</ModalHeader>
                    <ModalBody>
                        <LocalForm
                            onSubmit={values => this.handleSubmit(values)}
                        >
                            <Row className="form-group">
                                <Label htmlFor="firstname" md={3}>
                                    Rating
                                </Label>
                                <Col md={9}>
                                    <Control.text
                                        model=".firstname"
                                        id="firstname"
                                        name="firstname"
                                        placeholder="Rating"
                                        className="form-control"
                                        validators={{
                                            required,
                                            minLength: minLength(3),
                                            maxLength: maxLength(15),
                                        }}
                                    />
                                    <Errors
                                        className="text-danger"
                                        model=".firstname"
                                        show="touched"
                                        messages={{
                                            required: "Required",
                                            minLength:
                                                "Must be greater than 2 characters",
                                            maxLength:
                                                "Must be 15 characters or less",
                                        }}
                                    />
                                </Col>
                            </Row>

                            <Row className="form-group">
                                <Label htmlFor="comment" md={3}>
                                    Comment
                                </Label>
                                <Col md={9}>
                                    <Control.textarea
                                        model=".comment"
                                        id="comment"
                                        name="comment"
                                        placeholder="Comment"
                                        className="form-control"
                                        validators={{
                                            required,
                                            minLength: minLength(3),
                                            maxLength: maxLength(15),
                                        }}
                                    />
                                    <Errors
                                        className="text-danger"
                                        model=".comment"
                                        show="touched"
                                        messages={{
                                            required: "Required",
                                            minLength:
                                                "Must be greater than 2 characters",
                                            maxLength:
                                                "Must be 15 characters or less",
                                        }}
                                    />
                                </Col>
                            </Row>
                        </LocalForm>
                    </ModalBody>
                    <ModalFooter>
                        <Button type="submit" color="primary">
                            Submit
                        </Button>
                    </ModalFooter>
                </Modal>
            </div>
        );
    }
}

export default ModelPopup;
