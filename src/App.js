import React, { Component } from "react";
import Main from "./components/mainComponent";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import { configureStore } from "./redux/configStore";
const store = configureStore();

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router>
                    <div className="App">
                        <Main />
                    </div>
                </Router>
            </Provider>
        );
    }
}

export default App;
